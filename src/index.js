import { from, fromEvent, interval, map, mergeAll, mergeMap, mergeWith, startWith, takeUntil, of, endWith, merge} from "rxjs"

const canvas = document.getElementById("reactive-canvas");
const restartButton = document.getElementById("restart-button");
const cursorPosition = {x: 0, y: 0};

const updateCursorPosition = (event) => {
  cursorPosition.x = event.clientX - canvas.offsetLeft;
  cursorPosition.y = event.clientY - canvas.offsetTop;
  console.log(cursorPosition)
}
const onMouseDown$ = fromEvent(canvas, "mousedown");
let onMouseDownSubscription =  onMouseDown$.subscribe(updateCursorPosition);

const onMouseUp$ = fromEvent(canvas, "mouseup");
const onMouseMove$ = fromEvent(canvas, "mousemove").pipe(takeUntil(onMouseUp$));

// onMouseDown$.subscribe();


const canvasContext = canvas.getContext("2d");
canvasContext.lineWidth = 8;
canvasContext.lineJoin = "round";
canvasContext.lineCap = "round";
canvasContext.strokeStyle = "white";

const paintStroke = (event) => {

  canvasContext.beginPath();
  canvasContext.moveTo(cursorPosition.x, cursorPosition.y);
  updateCursorPosition(event)
  canvasContext.lineTo(cursorPosition.x, cursorPosition.y);
  canvasContext.stroke ();
  canvasContext.closePath();
}

const startPaint$ = onMouseDown$.pipe(
  map(() => onMouseMove$),
  mergeAll(onMouseMove$)
)

let startPaintSubscription =  startPaint$.subscribe(paintStroke);
restartButton.onLoadWI
// startPaint$.subscribe(console.log)
const onLoadWindow$ = fromEvent(window, 'load');
const onRestartClick$ = fromEvent(restartButton, 'click');

const restartWhiteBoard$ = merge(onLoadWindow$, onRestartClick$);
restartWhiteBoard$.subscribe(() => {
  startPaintSubscription.unsubscribe();
  onMouseDownSubscription.unsubscribe();
  canvasContext.clearRect(0, 0, canvas.width, canvas.height);
  startPaintSubscription = startPaint$.subscribe(paintStroke);
  onMouseDownSubscription = onMouseDown$.subscribe(updateCursorPosition);

});
